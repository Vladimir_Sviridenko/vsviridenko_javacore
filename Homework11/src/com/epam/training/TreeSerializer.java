package com.epam.training;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class TreeSerializer {
    private String filePath;

    public TreeSerializer(String filePath) {
	setFilePath(filePath);
    }

    public void setFilePath(String filePath) {
	if (filePath != null) {
	    this.filePath = filePath;
	} else {
	    throw new IllegalArgumentException("filePath can't be null");
	}
    }

    public String getFilePath() {
	return this.filePath;
    }
    
    public void saveTree(Tree tree) throws IOException {
	FileOutputStream fs = new FileOutputStream(filePath);
	ObjectOutputStream os = new ObjectOutputStream(fs);
	os.writeObject(tree);
	os.close();
    }

    public Tree loadTree() throws IOException, ClassNotFoundException {
	FileInputStream fis = new FileInputStream(filePath);
	ObjectInputStream ois = new ObjectInputStream(fis);
	Tree tree = (Tree) ois.readObject();
	ois.close();
	return tree;
    }
}
