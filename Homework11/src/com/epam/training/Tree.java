package com.epam.training;

import java.io.Serializable;

public class Tree implements Serializable {
    private Folder root = new Folder("root");

    public Folder getRoot() {
	return root;
    }

    public void setRoot(Folder root) {
	if (root == null) {
	    throw new IllegalArgumentException("root can't be null");
	} else {
	    this.root = root;
	}
    }
}
