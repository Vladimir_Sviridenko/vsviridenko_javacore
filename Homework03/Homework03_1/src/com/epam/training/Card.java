package com.epam.training;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Card {

    private String holderName;
    private BigDecimal balance;

    public Card(String holderName, BigDecimal balance) {
	if (holderName.isEmpty() || holderName == null)
	    throw new NullPointerException("holderName can't be empty or null");
	else
	    this.holderName = holderName;

	this.balance = balance.setScale(2, RoundingMode.HALF_UP);
    }

    public Card(String holderName) {
	this.holderName = holderName;
	this.balance = new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);
    }

    public String getHolderName() {
	return holderName;
    }

    public BigDecimal getBalance() {
	return balance;
    }

    public void increaseBalance(BigDecimal augend) {
	this.balance = balance.add(augend).setScale(2, RoundingMode.HALF_UP);
    }

    public void decreaseBalance(BigDecimal subtrahend) {
	this.balance = balance.subtract(subtrahend).setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal getConvertedBalacne(BigDecimal conversion) {
	return balance.multiply(conversion).setScale(2, RoundingMode.HALF_UP);
    }
}