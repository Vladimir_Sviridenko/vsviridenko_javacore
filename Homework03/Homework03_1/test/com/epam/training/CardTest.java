package com.epam.training;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.Test;
import org.junit.Assert;

/**
 * Test suite for Card class.
 */
public class CardTest {
    @Test
    public void testIncreaseBalance() {
	Card card = new Card("Alex Smith");
	card.increaseBalance(new BigDecimal(10.30));
	BigDecimal answer = new BigDecimal(10.30).setScale(2, RoundingMode.HALF_UP);

	Assert.assertEquals(answer, card.getBalance());
    }

    @Test
    public void testDecreaseBalance() {
	Card card = new Card("Alex Smith", new BigDecimal(27.00));
	card.decreaseBalance(new BigDecimal(9.30));
	BigDecimal answer = new BigDecimal(17.70).setScale(2, RoundingMode.HALF_UP);

	Assert.assertEquals(answer, card.getBalance());
    }

    @Test
    public void testConvertedBalance() {
	Card card = new Card("Alex Smith", new BigDecimal(30.00));
	BigDecimal convertedBalance = card.getConvertedBalacne(new BigDecimal(2.33));
	BigDecimal answer = new BigDecimal(69.90).setScale(2, RoundingMode.HALF_UP);

	Assert.assertEquals(answer, convertedBalance);
    }

}
