package com.epam.training.utils;

import java.util.Arrays;

public class Median {
    static public float median(int[] array) {
	float median;

	Arrays.sort(array);
	if (array.length % 2 == 0)
	    median = ((float) array[array.length / 2] + (float) array[array.length / 2 - 1]) / 2;
	else
	    median = (float) array[array.length / 2];

	return median;
    }

    static public double median(double[] array) {
	double median;

	Arrays.sort(array);
	if (array.length % 2 == 0)
	    median = ((double) array[array.length / 2] + (double) array[array.length / 2 - 1]) / 2;
	else
	    median = (double) array[array.length / 2];

	return median;
    }
}
