package com.epam.training;

import com.epam.training.exc.ValidationFailedException;

public interface Validator<T> {
    public void validate(T input) throws ValidationFailedException;
}
