package com.epam.training;

public class ValidationSystem {

    // Add your Validator here
    private static IntegerValidator integerValidator = new IntegerValidator();
    private static StringValidator stringValidator = new StringValidator();

    // Add your validate method here
    public static void validate(String string) {
	stringValidator.validate(string);
    }

    public static void validate(Integer integer) {
	integerValidator.validate(integer);
    }
}
