package com.epam.training.exc;

public class ValidationFailedException extends RuntimeException {
    public ValidationFailedException(String message) {
	super(message);
    }
}
