package com.epam.training;

import com.epam.training.exc.ValidationFailedException;

public class StringValidator implements Validator<String> {
    public void validate(String string) throws ValidationFailedException {
	if (string == null) {
	    throw new ValidationFailedException("string can't be null");
	}
	if (!string.matches("^[A-Z].*")) {
	    throw new ValidationFailedException("string must have the first letter in Uppercase");
	}
    }
}
