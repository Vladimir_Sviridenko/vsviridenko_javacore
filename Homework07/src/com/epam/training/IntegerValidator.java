package com.epam.training;

import com.epam.training.exc.ValidationFailedException;

public class IntegerValidator implements Validator<Integer> {
    public void validate(Integer integer) throws ValidationFailedException {
	if (integer == null) {
	    throw new ValidationFailedException("integer can't be null");
	}
	if (integer < 1 || integer > 10) {
	    throw new ValidationFailedException("integer must be greater 1 and less 10");
	}
    }
}
