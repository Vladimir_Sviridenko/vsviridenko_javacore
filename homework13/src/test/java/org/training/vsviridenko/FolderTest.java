package org.training.vsviridenko;

import org.junit.Assert;
import org.junit.Test;

import org.training.vsviridenko.exc.ExistingComponentException;

public class FolderTest {
    @Test
    public void testContainsFolder_True() {
	Folder parent = new Folder("parent");
	Folder child = new Folder("child");
	parent.addFolder(child);
	boolean actual = parent.containsFolder("child");

	boolean expected = true;
	Assert.assertEquals(expected, actual);
    }

    @Test
    public void testContainsFile_True() {
	Folder parent = new Folder("parent");
	File child = new File("childFile", "exe");
	parent.addFile(child);
	boolean actual = parent.containsFile(child.getName(), child.getExtension());

	boolean expected = true;
	Assert.assertEquals(expected, actual);
    }

    @Test
    public void testContainsFolder_False() {
	Folder parent = new Folder("parent");
	File child = new File("childFile", "exe");
	parent.addFile(child);

	File notChild = new File("notChildFile", "exe");
	boolean actual = parent.containsFile(notChild.getName(), notChild.getExtension());
	
	boolean expected = false;
	Assert.assertEquals(expected, actual);
    }

    @Test
    public void testContainsFile_False() {
	Folder parent = new Folder("parent");
	File child = new File("childFile", "exe");
	parent.addFile(child);

	File notChildFile = new File("notChildFile", "exe");
	boolean actual = parent.containsFile(notChildFile.getName(), notChildFile.getExtension());

	boolean expected = false;
	Assert.assertEquals(expected, actual);
    }

}
