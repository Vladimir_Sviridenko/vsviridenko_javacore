package org.training.vsviridenko.exc;

/**<p>ExistingComponentException</p>
 * throws when component(file or folder) is existing*/

public class ExistingComponentException extends Exception {
    public ExistingComponentException(String message) {
	super(message);
    }
}
