package org.training.vsviridenko;

import java.io.Serializable;

public class File implements Serializable{
    private String name;
    private String extension;

    public File(String name, String extension) {
	setName(name);
	setExtension(extension);
    }

    public String getName() {
	return name;
    }

    private void setName(String name) {
	if (name == null || name.isEmpty()) {
	    throw new IllegalArgumentException("name can't be null or empty");
	} else {
	    this.name = name;
	}
    }

    public String getExtension() {
	return extension;
    }

    public void setExtension(String extension) {
	if (extension == null || name.isEmpty())
	    throw new IllegalArgumentException("extension can't be null or empty");
	else
	    this.extension = extension;
    }

    @Override
    public String toString() {
	return this.getName() + "." + this.getExtension();
    }
}
