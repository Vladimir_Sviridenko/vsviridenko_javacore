package org.training.vsviridenko;

public class Launcher {
    public static void main(String[] args) {
	Tree tree = new Tree();
	UserInteraction.startDialog(tree);
    }
}
