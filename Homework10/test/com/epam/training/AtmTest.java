package com.epam.training;

import org.junit.Assert;
import org.junit.Test;

public class AtmTest {
   
    @Test
    public void testIncreaseBalance0() {
	Card card = new Card(600);
	MoneyProducer moneyProducer1 = new MoneyProducer(card);
	moneyProducer1.increaseBalance(100);
	
	int expection = 700;
	Assert.assertEquals(expection, card.getBalance());
    }
    
    @Test
    public void testIncreaseBalance1() {
	Card card = new Card(400);
	MoneyProducer moneyProducer1 = new MoneyProducer(card);
	moneyProducer1.increaseBalance(200);
	
	int expection = 600;
	Assert.assertEquals(expection, card.getBalance());
    }
    
    @Test
    public void testDecreaseBalance0() {
	Card card = new Card(900);
	MoneyConsumer moneyConsumer1 = new MoneyConsumer(card);
	moneyConsumer1.decreaseBalance(100);
	
	int expection = 800;
	Assert.assertEquals(expection, card.getBalance());
    }
    
    @Test
    public void testDecreaseBalance1() {
	Card card = new Card(600);
	MoneyConsumer moneyConsumer1 = new MoneyConsumer(card);
	moneyConsumer1.decreaseBalance(300);
	
	int expection = 300;
	Assert.assertEquals(expection, card.getBalance());
    }
}
