package com.epam.training;

public class MoneyConsumer implements Runnable {
    private Card card;

    public MoneyConsumer(Card card) {
	this.card = card;
    }

    @Override
    public void run() {
	while (true) {
	    synchronized (card) {
		if (card.getBalance() > 0 && card.getBalance() < 1000) {
		    decreaseBalance(100);
		} else {
		    break;
		}
	    }
	}
    }
    
    public void decreaseBalance(int decrement) {
	try {
	    Thread.sleep(1000);
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
	System.out.println(Thread.currentThread().getName() + " going to decrease balance.");
	try {
	    Thread.sleep(2000);
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
	card.decreaseBalance(decrement);
	System.out.println(Thread.currentThread().getName() + " completed decreasing.");
	System.out.println("Current balance: " + card.getBalance() + "$");
	System.out.println();
    }
}
