package com.epam.training;

public class Launcher {
    public static void main(String[] args) {

	Card card = new Card(500);
	
	MoneyProducer moneyProducer1 = new MoneyProducer(card);
	MoneyProducer moneyProducer2 = new MoneyProducer(card);
	MoneyConsumer moneyConsumer1 = new MoneyConsumer(card);
	MoneyConsumer moneyConsumer2 = new MoneyConsumer(card);
	
	Thread tProducer1 = new Thread(moneyProducer1);
	Thread tProducer2 = new Thread(moneyProducer2);
	Thread tConsumer1 = new Thread(moneyConsumer1);
	Thread tConsumer2 = new Thread(moneyConsumer2);
	
	tProducer1.setName("+MoneyProducer1");
	tProducer2.setName("+MoneyProducer2");
	tConsumer1.setName("-MoneyConsumer1");
	tConsumer2.setName("-MoneyConsumer2");

	tProducer1.start();
	tProducer2.start();
	tConsumer1.start();
	tConsumer2.start();
	
	try {
	    tProducer1.join();
	    tProducer2.join();
	    tConsumer1.join();
	    tConsumer2.join();
	} catch (InterruptedException e) {
	    System.out.println("Thread exception!");
	}
	
	System.out.println("Card balance reached limit. Balance: " + card.getBalance() + "$");
    }
}
