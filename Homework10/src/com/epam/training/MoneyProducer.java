package com.epam.training;

public class MoneyProducer implements Runnable {
    private Card card;

    public MoneyProducer(Card card) {
	this.card = card;
    }

    @Override
    public void run() {
	while (true) {
	    synchronized (card) {
		if (card.getBalance() > 0 && card.getBalance() < 1000) {
		    increaseBalance(100);
		} else {
		    break;
		}
	    }
	}
    }

    public void increaseBalance(int increment) {
	try {
	    Thread.sleep(1000);
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
	System.out.println(Thread.currentThread().getName() + " going to increase balance.");
	try {
	    Thread.sleep(2000);
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
	card.increaseBalance(increment);
	System.out.println(Thread.currentThread().getName() + " completed increasing.");
	System.out.println("Current balance: " + card.getBalance() + "$");
	System.out.println();
    }
}
