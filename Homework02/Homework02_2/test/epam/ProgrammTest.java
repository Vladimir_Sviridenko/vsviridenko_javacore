package epam;

import org.junit.Assert;
import org.junit.Test;

public class ProgrammTest {

	/* FIBONACCI testing: All loopTypes */

	@Test
	public void testCalcFibonacciSequence_WhileType() {
		int[] fibonacciSeqence = Programm.calcFibonacciSequence(1, 1);
		int[] answer = { 1 };
		Assert.assertArrayEquals(answer, fibonacciSeqence);
	}

	@Test
	public void testCalcFibonacciSequence_DoWhileType() {
		int[] fibonacciSeqence = Programm.calcFibonacciSequence(2, 2);
		int[] answer = { 1, 1 };
		Assert.assertArrayEquals(answer, fibonacciSeqence);
	}

	@Test
	public void testCalcFibonacciSequence_ForType() {
		int[] fibonacciSeqence = Programm.calcFibonacciSequence(7, 3);
		int[] answer = { 1, 1, 2, 3, 5, 8, 13 };
		Assert.assertArrayEquals(answer, fibonacciSeqence);
	}

	/* FACTORIAL testing: All loopTypes */

	@Test
	public void testCalcFactorial_WhileType() {
		int factorial = Programm.calcFactorial(5, 1);
		Assert.assertEquals(120, factorial);
	}

	@Test
	public void testCalcFactorial_DoWhileType() {
		int factorial = Programm.calcFactorial(6, 2);
		Assert.assertEquals(720, factorial);
	}

	@Test
	public void testCalcFactorial_ForType() {
		int factorial = Programm.calcFactorial(7, 3);
		Assert.assertEquals(5040, factorial);
	}
}