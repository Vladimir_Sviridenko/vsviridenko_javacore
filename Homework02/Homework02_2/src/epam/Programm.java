package epam;

import java.util.Arrays;
import java.util.Scanner;

public class Programm {
	public static void main(String[] args) {

		int algorithmid = Integer.parseInt(args[0]);
		int loopType = Integer.parseInt(args[1]);
		int n = Integer.parseInt(args[2]);
		
		try {
			switch (algorithmid) {
			case 1:
				int[] result = calcFibonacciSequence(n, loopType);
				System.out.println(Arrays.toString(result));
				break;

			case 2:
				System.out.println(calcFactorial(n, loopType));
				break;

			default:
				System.out.println("unknown algorithmId");
			}
		} catch (RuntimeException exc) {
			System.out.println(exc.getMessage());
		}
	}

	public static int[] calcFibonacciSequence(int n, int loopType) throws RuntimeException {

		if (n == 1)
			return new int[] { 1 };
		if (n == 2)
			return new int[] { 1, 1 };

		int[] result = new int[n];
		int fibNumber;
		int prev = 1;
		int next = 1;
		// counter for while and do-while
		int counter = 2;

		result[0] = prev;
		result[1] = next;

		switch (loopType) {
		case 1:
			while (counter < result.length) {
				fibNumber = prev + next;
				prev = next;
				next = fibNumber;
				result[counter] = fibNumber;
				counter++;
			}
			break;

		case 2:
			do {
				fibNumber = prev + next;
				prev = next;
				next = fibNumber;
				result[counter] = fibNumber;
				counter++;
			} while (counter < result.length);
			break;

		case 3:
			for (int i = 2; i < result.length; i++) {
				fibNumber = prev + next;
				prev = next;
				next = fibNumber;
				result[i] = fibNumber;
			}
			break;

		default:
			throw new RuntimeException("LoopType is wrong. Loop Type should be 1-3.");
		}

		return result;
	}

	public static int calcFactorial(int n, int loopType) throws RuntimeException {
		int result = 1;
		// counter for while and do-while
		int counter = 1;

		switch (loopType) {
		case 1:
			while (counter <= n) {
				result = result * counter;
				counter++;
			}
			break;

		case 2:
			do {
				result = result * counter;
				counter++;
			} while (counter <= n);
			break;

		case 3:
			for (int i = 1; i <= n; i++) {
				result = result * i;
			}
			break;

		default:
			throw new RuntimeException("LoopType is wrong. Loop Type should be 1-3.");
		}

		return result;
	}
}