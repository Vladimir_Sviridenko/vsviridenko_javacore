package epam;

import org.junit.Assert;
import org.junit.Test;

public class CoefficientCalculatorTest {
	@Test
	public void testCalcG_1111() {
		double G = CoefficientCalculator.calcG(1, 1, 1, 1);
		Assert.assertEquals(2 * Math.pow(Math.PI, 2), G, 0.01);
	}
	@Test
	public void testCalcG_doubleM1M2() {
		double G = CoefficientCalculator.calcG(5, 2, 2.3, 3.5);
		Assert.assertEquals(21.551 * Math.pow(Math.PI, 2), G, 0.01);
	}
	@Test
	public void testCalcG_Infinity() {
		double G = CoefficientCalculator.calcG(1, 1, 0, 0);
		Assert.assertEquals(Double.POSITIVE_INFINITY, G, 0);
	}
}