package epam;

import java.lang.Math;

public class CoefficientCalculator {

	public static void main(String[] args) {

		int a = Integer.parseInt(args[0]);
		int p = Integer.parseInt(args[1]);
		double m1 = Double.parseDouble(args[2]);
		double m2 = Double.parseDouble(args[3]);

		System.out.println(calcG(a, p, m1, m2));
	}

	/* calculate coefficient G */
	public static double calcG(int a, int p, double m1, double m2) {
		double G;
		G = 4 * Math.pow(Math.PI, 2) * (Math.pow(a, 3) / (Math.pow(p, 2) * (m1 + m2)));
		return G;
	}
}