package com.epam.training;

public class SortingContext {

	private Sorter sortStrategy;

	public SortingContext(Sorter sortStrategy) {
		if (sortStrategy != null)
			this.sortStrategy = sortStrategy;
		else
			throw new IllegalArgumentException("sortStrategy can't be null");
	}

	public int[] execute(int[] array) {
		sortStrategy.sort(array);
		return array;
	}
}
