package com.epam.training;

import java.util.Arrays;

public class Sorter {

	protected void sort(int[] array) {
		Arrays.sort(array);
	}

	public static void swapNumbers(int i, int j, int[] array) {
		int temp;
		temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
}
