package com.epam.training;

import org.junit.Assert;
import org.junit.Test;

public class SorterTest {

	@Test
	public void testBubbleSort() {
		int[] array = { 4, 5, 3, 7, 86, 16 };
		Sorter sortStrategy = new BubbleSort();
		SortingContext context = new SortingContext(sortStrategy);
		context.execute(array);
		int[] result = { 3, 4, 5, 7, 16, 86 };

		Assert.assertArrayEquals(result, array);
	}

	@Test
	public void testSelectionSort() {
		int[] array = { 4, 5, 3, 7, 86, 16 };
		Sorter sortStrategy = new SelectionSort();
		SortingContext context = new SortingContext(sortStrategy);
		context.execute(array);
		int[] result = { 3, 4, 5, 7, 16, 86 };

		Assert.assertArrayEquals(result, array);
	}
}
