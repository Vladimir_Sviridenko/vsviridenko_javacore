package com.epam.training.exc;

/**
 * <p>NegativeBalanceException</p> 
 * throws when balance is negative
 */
public class NegativeBalanceException extends RuntimeException {
	public NegativeBalanceException(String message) {
		super(message);
	}
}
