package com.epam.training;

import java.math.BigDecimal;

public class Atm {
	private Card card;

	public Atm(Card card) {
		this.card = card;
	}

	public void setCard(Card card) {
		if (card == null)
			throw new IllegalArgumentException("card can't be null");
		else
			this.card = card;
	}

	public Card getCard() {
		return this.card;
	}

	public void enterMoney(BigDecimal increment) {
		card.increaseBalance(increment);
	}

	public void withdrawMoney(BigDecimal decrement) {
		card.decreaseBalance(decrement);
	}

}
