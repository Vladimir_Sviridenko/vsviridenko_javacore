package com.epam.training;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.epam.training.exc.NegativeBalanceException;

public class CreditCard extends Card {

    private BigDecimal maxDebt = new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);

    public CreditCard(String holderName, BigDecimal balance, BigDecimal maxDebt) {
	super(holderName, balance);
	setMaxDebt(maxDebt.setScale(2, RoundingMode.HALF_UP));
    }
    
    public CreditCard(String holderName, BigDecimal balance) {
	super(holderName, balance);
    }

    public CreditCard(String holderName) {
	super(holderName);
    }

    public BigDecimal getMaxDebt() {
	return this.maxDebt;
    }

    public void setMaxDebt(BigDecimal maxDebt) {
	if (maxDebt == null)
	    throw new IllegalArgumentException("debt can't be null");
	else
	    this.maxDebt = maxDebt;
    }

    @Override
    protected void decreaseBalance(BigDecimal decrement) {
	if (getBalance().subtract(decrement).doubleValue() >= getMaxDebt().doubleValue())
	    super.decreaseBalance(decrement);
	else
	    throw new NegativeBalanceException("You have not enough money on balance");
    }
}
