package com.epam.training;

import java.math.BigDecimal;

import com.epam.training.exc.NegativeBalanceException;

public class DebitCard extends Card {

    public DebitCard(String holderName, BigDecimal balance) {
	super(holderName, balance);
    }

    public DebitCard(String holderName) {
	super(holderName);
    }

    @Override
    protected void setBalance(BigDecimal balance) {
	if (balance.doubleValue() < 0)
	    throw new NegativeBalanceException("debit card can't have negative balance");
	else
	    super.setBalance(balance);
    }

    @Override
    protected void decreaseBalance(BigDecimal decrement) {
	if (getBalance().subtract(decrement).doubleValue() >= 0)
	    super.decreaseBalance(decrement);
	else
	    throw new NegativeBalanceException("You have not enough money on balance");
    }
}
