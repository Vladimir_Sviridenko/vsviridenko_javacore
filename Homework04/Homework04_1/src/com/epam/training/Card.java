package com.epam.training;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Card {

	protected String holderName;
	protected BigDecimal balance = new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);

	public Card(String holderName, BigDecimal balance) {
		setHolderName(holderName);
		setBalance(balance.setScale(2, RoundingMode.HALF_UP));
	}

	public Card(String holderName) {
		setHolderName(holderName);
	}

	public String getHolderName() {
		return holderName;
	}

	protected void setHolderName(String holderName) {
		if (holderName.isEmpty() || holderName == null)
			throw new IllegalArgumentException("holderName can't be empty or null");
		else
			this.holderName = holderName;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	protected void setBalance(BigDecimal balance) {
		if (balance == null)
			throw new IllegalArgumentException("balance can't be null");
		else
			this.balance = balance;
	}

	protected void increaseBalance(BigDecimal increment) {
		setBalance(getBalance().add(increment).setScale(2, RoundingMode.HALF_UP));
	}

	protected void decreaseBalance(BigDecimal decrement) {
		setBalance(getBalance().subtract(decrement).setScale(2, RoundingMode.HALF_UP));
	}

	public BigDecimal getConvertedBalacne(BigDecimal conversion) {
		return getBalance().multiply(conversion).setScale(2, RoundingMode.HALF_UP);
	}
}