package com.epam.training;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.Assert;
import org.junit.Test;

public class AtmTest {

	@Test
	public void testEnterMoney() {
		Atm atm = new Atm(new DebitCard("Alex Smith"));
		atm.enterMoney(new BigDecimal(20));
		BigDecimal answer = new BigDecimal(20).setScale(2, RoundingMode.HALF_UP);

		Assert.assertEquals(answer, atm.getCard().getBalance());
	}

	@Test
	public void testWithdrawMoney() {
		Atm atm = new Atm(new CreditCard("Alex Smith", new BigDecimal(40)));
		atm.withdrawMoney(new BigDecimal(30));
		BigDecimal answer = new BigDecimal(10).setScale(2, RoundingMode.HALF_UP);

		Assert.assertEquals(answer, atm.getCard().getBalance());
	}
}
