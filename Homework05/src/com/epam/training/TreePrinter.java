package com.epam.training;

public class TreePrinter {
    public static void printTree(Tree tree) {
	printFolder(tree.getRoot(), 0);
    }

    private static void printFolder(Folder folder, int indent) {
	printIndent(indent);
	System.out.println(folder.getName() + "");
	indent++;

	for (Folder tempFolder : folder.getChildFolders()) {
	    printFolder(tempFolder, indent);

	    for (File tempFile : folder.getChildFiles()) {
		printFile(tempFile, indent);
	    }
	}
    }

    private static void printIndent(int indent) {
	for (int i = 0; i < indent; i++) {
	    System.out.print("    ");
	}
    }

    private static void printFile(File file, int indent) {
	printIndent(indent);
	System.out.println(file.getName() + "." + file.getExtension());
    }
}