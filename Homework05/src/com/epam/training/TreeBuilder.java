package com.epam.training;

import com.epam.training.exc.ExistingComponentException;

public class TreeBuilder {
    public static void buildPath(Tree tree, String[] path) throws ExistingComponentException {
	Folder currentFolder = tree.getRoot();

	// check for File in the path
	boolean pathHasFile = false;
	if (path[path.length - 1].contains(".")) {
	    pathHasFile = true;
	}

	// ignore same folders in the path
	int i = 1;
	while (i < path.length && currentFolder.containsFolder(path[i])) {
	    currentFolder = currentFolder.getChildFolderByName(path[i]);
	    i++;
	}

	// ADD FOLDERS
	if (i == path.length && !pathHasFile) {
	    throw new ExistingComponentException("Folder already exists.");
	} else {
	    while (i < path.length) {
		currentFolder.addFolder(new Folder(path[i]));
		currentFolder = currentFolder.getChildFolderByName(path[i]);
		i++;
	    }
	}

	// ADD FILE
	if (pathHasFile) {
	    String[] fileFullName = path[path.length - 1].split("\\.");
	    String fileName = fileFullName[0];
	    String fileExtension = fileFullName[1];

	    if (currentFolder.containsFile(fileName, fileExtension)) {
		throw new ExistingComponentException("File already exists.");
	    } else {
		currentFolder.addFile(new File(fileName, fileExtension));
	    }
	}
    }
}
