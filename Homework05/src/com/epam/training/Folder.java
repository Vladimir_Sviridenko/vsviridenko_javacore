package com.epam.training;

import java.util.ArrayList;

public class Folder {
    private String name;
    private ArrayList<Folder> childFolders = new ArrayList<Folder>();
    private ArrayList<File> childFiles = new ArrayList<File>();

    public Folder(String name) {
	setName(name);
    }

    public String getName() {
	return name;
    }

    private void setName(String name) {
	if (name == null || name.isEmpty()) {
	    throw new IllegalArgumentException("name can't be null or empty");
	} else {
	    this.name = name;
	}
    }

    public ArrayList<Folder> getChildFolders() {
	return childFolders;
    }

    public ArrayList<File> getChildFiles() {
	return childFiles;
    }

    public Folder getChildFolderByName(String childFolderName) {
	Folder result = null;

	for (Folder folder : childFolders) {
	    if (folder.getName().equals(childFolderName)) {
		result = folder;
	    }
	}
	return result;
    }

    public boolean containsFolder(String folderName) {
	boolean result = false;

	for (Folder temp : childFolders) {
	    if (temp.getName().equals(folderName)) {
		result = true;
	    }
	}
	return result;
    }

    public boolean containsFile(String fileName, String fileExtension) {
	boolean result = false;

	for (File temp : childFiles) {
	    if (temp.toString().equals(fileName + "." + fileExtension)) {
		result = true;
	    }
	}
	return result;
    }

    public void addFolder(Folder folder) {
	childFolders.add(folder);
    }

    public void addFile(File file) {
	childFiles.add(file);
    }

    @Override
    public String toString() {
	return this.getName();
    }
}
