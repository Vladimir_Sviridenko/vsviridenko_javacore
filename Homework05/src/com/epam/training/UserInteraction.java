package com.epam.training;

import java.util.Scanner;

import com.epam.training.exc.ExistingComponentException;

public class UserInteraction {

    public static void printHelp() {
	System.out.println("Available commands:");
	System.out.println("'root/example/example.txt' - adds path to the file system");
	System.out.println("'help' - displays available commands");
	System.out.println("'print' - displays current file system.");
	System.out.println("'exit' - exits the program.");
    }

    public static void startDialog(Tree tree) {
	printHelp();
	Scanner in = new Scanner(System.in);
	String userInput = in.nextLine();
	while (!userInput.equals("exit")) {
	    switch (userInput) {
		case "help":
		    printHelp();
		    break;
		case "print":
		    TreePrinter.printTree(tree);
		    break;
		default:
		    String[] path = splitPath(userInput);
		    /* check for first folder is ROOT */
		    if (path[0].equals("root")) {
			try {
			    TreeBuilder.buildPath(tree, path);
			} catch (ExistingComponentException e) {
			    System.out.println(e.getMessage() + " Try again.");
			}
		    } else {
			System.out.println("First folder should be 'root'. Try again.");
		    }
		    break;
	    }
	    userInput = in.nextLine();
	}
	in.close();
    }

    private static String[] splitPath(String path) {
	String[] result = path.split("/");
	return result;
    }
}
