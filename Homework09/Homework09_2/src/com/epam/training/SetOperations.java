package com.epam.training;

import java.util.HashSet;

public class SetOperations {

    public static <T> HashSet<T> getUnion(HashSet<T> set1, HashSet<T> set2) {
	HashSet<T> result = new HashSet<T>();
	result.addAll(set1);
	result.addAll(set2);
	return result;
    }

    public static <T> HashSet<T> getIntersection(HashSet<T> set1, HashSet<T> set2) {
	HashSet<T> result = new HashSet<T>();
	result.addAll(set1);
	result.retainAll(set2);
	return result;
    }

    public static <T> HashSet<T> getSubtraction(HashSet<T> set1, HashSet<T> set2) {
	HashSet<T> result = new HashSet<T>();
	result.addAll(set1);
	result.removeAll(set2);
	return result;
    }

    public static <T> HashSet<T> getExclusion(HashSet<T> set1, HashSet<T> set2) {
	HashSet<T> result = new HashSet<T>();
	result = getUnion(set1, set2);
	result.removeAll(getIntersection(set1, set2));
	return result;
    }

}
