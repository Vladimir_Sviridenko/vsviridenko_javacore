package com.epam.training;

import java.util.HashSet;

import org.junit.Assert;
import org.junit.Test;

public class SetOperationsTest {

    HashSet<Integer> set1 = new HashSet<Integer>();
    HashSet<Integer> set2 = new HashSet<Integer>();
    {
	set1.add(30);
	set1.add(70);
	set1.add(9);
	set1.add(13);

	set2.add(10);
	set2.add(28);
	set2.add(9);
	set2.add(13);
    }

    @Test
    public void testGetUnion() {
	HashSet<Integer> expected = new HashSet<Integer>();
	expected.add(30);
	expected.add(70);
	expected.add(9);
	expected.add(13);

	expected.add(10);
	expected.add(28);
	expected.add(9);
	expected.add(13);

	Assert.assertEquals(expected, SetOperations.getUnion(set1, set2));
    }

    @Test
    public void testGetIntersection() {
	HashSet<Integer> expected = new HashSet<Integer>();
	expected.add(9);
	expected.add(13);

	Assert.assertEquals(expected, SetOperations.getIntersection(set1, set2));
    }

    @Test
    public void testGetSubtraction() {
	HashSet<Integer> expected = new HashSet<Integer>();
	expected.add(70);
	expected.add(30);

	Assert.assertEquals(expected, SetOperations.getSubtraction(set1, set2));
    }

    @Test
    public void testGetExclusion() {
	HashSet<Integer> expected = new HashSet<Integer>();
	expected.add(70);
	expected.add(30);
	expected.add(28);
	expected.add(10);

	Assert.assertEquals(expected, SetOperations.getExclusion(set1, set2));
    }

}
