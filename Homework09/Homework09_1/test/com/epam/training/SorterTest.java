package com.epam.training;

import org.junit.Assert;
import org.junit.Test;

public class SorterTest {
    @Test
    public void testGetDigitsSum0() {
	Assert.assertEquals(15, Sorter.getDigitsSum(258));
    }

    @Test
    public void testGetDigitsSum1() {
	Assert.assertEquals(18, Sorter.getDigitsSum(7632));
    }

    @Test
    public void testSortByDigitsSum0() {
	int[] array = { 16, 24, 108, 1003 };
	Sorter.sortByDigitsSum(array);

	int[] expected = { 1003, 24, 16, 108 };

	Assert.assertArrayEquals(expected, array);
    }

    @Test
    public void testSortByDigitsSum1() {
	int[] array = { 0, 1, 30, 13 };
	Sorter.sortByDigitsSum(array);

	int[] expected = { 0, 1, 30, 13 };

	Assert.assertArrayEquals(expected, array);
    }

    @Test
    public void testSortByDigitsSum2() {
	int[] array = { 70, 123, 208, 12, 100 };
	Sorter.sortByDigitsSum(array);

	int[] expected = { 100, 12, 123, 70, 208 };

	Assert.assertArrayEquals(expected, array);
    }
}
