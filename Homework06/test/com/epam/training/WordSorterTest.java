package com.epam.training;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

public class WordSorterTest {
    @Test
    public void testGetWordsGroupByAlphabet_OneWord() {
	String text = "Hello";
	WordSorter sorter = new WordSorter(text);
	ArrayList<String> wordGroups = sorter.getWordsGroupByAlphabet();

	ArrayList<String> answer = new ArrayList<String>();
	answer.add("H:hello");

	Assert.assertEquals(answer, wordGroups);
    }

    @Test
    public void testGetWordsGroupByAlphabet_TwoWords() {
	String text = "Hello,@ World!!";
	WordSorter sorter = new WordSorter(text);
	ArrayList<String> wordGroups = sorter.getWordsGroupByAlphabet();

	ArrayList<String> answer = new ArrayList<String>();
	answer.add("H:hello");
	answer.add("W:world");

	Assert.assertEquals(answer, wordGroups);
    }

    @Test
    public void testGetWordsGroupByAlphabet_BigText() {
	String text = "Once upon a time a Wolf was lapping at a spring on a hillside,"
		+ " when, looking up, what should he see but a Lamb just beginning"
		+ " to drink a little lower down.";
	WordSorter sorter = new WordSorter(text);
	ArrayList<String> wordGroups = sorter.getWordsGroupByAlphabet();

	ArrayList<String> answer = new ArrayList<String>();
	answer.add("A:a at");
	answer.add("B:beginning but");
	answer.add("D:down drink");
	answer.add("H:he hillside");
	answer.add("J:just");
	answer.add("L:lamb lapping little looking lower");
	answer.add("O:on once");
	answer.add("S:see should spring");
	answer.add("T:time to");
	answer.add("U:up upon");
	answer.add("W:was what when wolf");
	
	Assert.assertEquals(answer, wordGroups);
    }
    
    @Test
    public void testGetWordsGroupByAlphabet_Punctuation() {
	String text = "looking:;%5 up***(), what%� should :::he see but%�4 a Lamb ";
	WordSorter sorter = new WordSorter(text);
	ArrayList<String> wordGroups = sorter.getWordsGroupByAlphabet();

	ArrayList<String> answer = new ArrayList<String>();
	answer.add("A:a");
	answer.add("B:but");
	answer.add("H:he");
	answer.add("L:lamb looking");
	answer.add("S:see should");
	answer.add("U:up");
	answer.add("W:what");
	
	Assert.assertEquals(answer, wordGroups);
    }
}
