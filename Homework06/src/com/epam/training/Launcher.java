package com.epam.training;

import java.util.ArrayList;
import java.util.Scanner;

public class Launcher {
    public static void main(String[] args) {

	Scanner in = new Scanner(System.in);
	String text = in.nextLine();
	in.close();
	
	WordSorter wordSorter = new WordSorter(text);

	ArrayList<String> wordGroups = wordSorter.getWordsGroupByAlphabet();
	for (String group : wordGroups) {
	    System.out.println(group);
	}

    }
}
