package com.epam.training;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class WordSorter {
    private String text;

    public WordSorter(String text) {
	setText(text);
    }

    public String getText() {
	return text;
    }

    public void setText(String text) {
	if (text == null) {
	    throw new IllegalArgumentException("text can't be null");
	} else {
	    this.text = text.toLowerCase();
	}
    }

    public String[] getWordSet() {
	String[] wordsFromText = this.text.split("[^a-zA-Z]+");
	/* set of all words */
	Set<String> wordSet = new HashSet<String>(Arrays.asList(wordsFromText));
	/* Array of all words without repeat (convert from set to array) */
	String[] result = wordSet.toArray(new String[wordSet.size()]);
	return result;
    }

    public String[] getSortedWordSet() {

	String[] words = getWordSet();
	Arrays.sort(words);

	return words;
    }

    public ArrayList<String> getWordsGroupByAlphabet() {

	/* All word groups */
	ArrayList<String> groups = new ArrayList<String>();

	String[] words = getSortedWordSet();

	Boolean isNextGroup = true;
	String group = "";

	if (words.length == 1) {
	    char firstLetter = words[0].toCharArray()[0];
	    group += Character.toUpperCase(firstLetter) + ":";
	    group += words[0];
	    groups.add(group);
	}

	for (int i = 1; i < words.length; i++) {
	    char firstLetter = words[i - 1].toCharArray()[0];
	    char nextWordFirstLetter = words[i].toCharArray()[0];

	    /* add letter of group */
	    if (isNextGroup) {
		group += Character.toUpperCase(firstLetter) + ":";
		isNextGroup = false;
	    } else {

	    }

	    /* add current word to the group */
	    group += words[i - 1];

	    if (firstLetter != nextWordFirstLetter) {
		groups.add(group);
		isNextGroup = true;
		group = "";
	    } else {
		group += " ";
	    }

	    /* add last word group */
	    if (i == words.length - 1) {
		if (firstLetter != nextWordFirstLetter) {
		    group += Character.toUpperCase(nextWordFirstLetter) + ":";
		}
		group += words[i];
		groups.add(group);
	    }
	}
	return groups;
    }
}
