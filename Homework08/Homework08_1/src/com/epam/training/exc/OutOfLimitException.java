package com.epam.training.exc;

/**
 * <p>
 * OutOfLimitException
 * </p>
 * throws when list is over filled
 */
@SuppressWarnings("serial")
public class OutOfLimitException extends RuntimeException {
    public OutOfLimitException(String message) {
	super(message);
    }
}
