package com.epam.training;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import com.epam.training.exc.OutOfLimitException;

public class LimitedListTest {

    @Test(expected = OutOfLimitException.class)
    public void testAdd_LimitException() {
	LimitedList<Integer> list = new LimitedList<Integer>(2);
	list.add(13);
	list.add(13);
	list.add(13);
    }

    @Test
    public void testAddByIndex() {
	LimitedList<Integer> listActual = new LimitedList<Integer>(4);
	listActual.add(0);
	listActual.add(1);
	listActual.add(2);
	listActual.add(1, 13);

	LimitedList<Integer> listExpected = new LimitedList<Integer>(4);
	listExpected.add(0);
	listExpected.add(13);
	listExpected.add(1);
	listExpected.add(2);

	Assert.assertArrayEquals(listExpected.toArray(), listActual.toArray());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testAddByIndex_IndexException() {
	LimitedList<Integer> list = new LimitedList<Integer>(4);
	list.add(0);
	list.add(1);
	list.add(2);
	list.add(4, 13);
    }

    @Test(expected = OutOfLimitException.class)
    public void testAddByIndex_LimitException() {
	LimitedList<Integer> list = new LimitedList<Integer>(4);
	list.add(0);
	list.add(1);
	list.add(2);
	list.add(3);
	list.add(2, 13);
    }

    @Test
    public void testAddAll() {

	LimitedList<Integer> listActual = new LimitedList<Integer>(6);
	listActual.add(0);
	listActual.add(1);
	listActual.add(2);

	ArrayList<Integer> collection = new ArrayList<Integer>();
	collection.add(13);
	collection.add(15);
	collection.add(16);
	listActual.addAll(collection);

	LimitedList<Integer> listExpected = new LimitedList<Integer>(6);
	listExpected.add(0);
	listExpected.add(1);
	listExpected.add(2);
	listExpected.add(13);
	listExpected.add(15);
	listExpected.add(16);

	Assert.assertArrayEquals(listExpected.toArray(), listActual.toArray());
    }

    @Test(expected = OutOfLimitException.class)
    public void testAddAll_LimitException() {

	LimitedList<Integer> listActual = new LimitedList<Integer>(4);
	listActual.add(0);
	listActual.add(1);
	listActual.add(2);

	ArrayList<Integer> collection = new ArrayList<Integer>();
	collection.add(13);
	collection.add(15);

	listActual.addAll(collection);
    }

    @Test
    public void testAddAllByIndex() {
	LimitedList<Integer> listActual = new LimitedList<Integer>(8);
	listActual.add(0);
	listActual.add(1);
	listActual.add(2);

	ArrayList<Integer> collection = new ArrayList<Integer>();
	collection.add(13);
	collection.add(15);
	collection.add(16);
	listActual.addAll(2, collection);

	LimitedList<Integer> listExpected = new LimitedList<Integer>(8);
	listExpected.add(0);
	listExpected.add(1);
	listExpected.add(13);
	listExpected.add(15);
	listExpected.add(16);
	listExpected.add(2);

	Assert.assertArrayEquals(listExpected.toArray(), listActual.toArray());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testAddAllByIndex_IndexException() {
	LimitedList<Integer> listActual = new LimitedList<Integer>(8);
	listActual.add(0);
	listActual.add(1);
	listActual.add(2);

	ArrayList<Integer> collection = new ArrayList<Integer>();
	collection.add(13);
	collection.add(15);
	collection.add(16);

	listActual.addAll(4, collection);
    }

    @Test(expected = OutOfLimitException.class)
    public void testAddAllByIndex_LimitException() {
	LimitedList<Integer> listActual = new LimitedList<Integer>(5);
	listActual.add(0);
	listActual.add(1);
	listActual.add(2);

	ArrayList<Integer> collection = new ArrayList<Integer>();
	collection.add(13);
	collection.add(15);
	collection.add(16);
	listActual.addAll(2, collection);
    }
}
