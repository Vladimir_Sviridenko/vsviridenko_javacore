package com.epam.training.exc;

/**
 * <p>
 * PickFailedException
 * </p>
 * throws when pick of team for work is failed
 */
@SuppressWarnings("serial")
public class PickFailedException extends Exception {
    public PickFailedException(String message) {
	super(message);
    }
}
