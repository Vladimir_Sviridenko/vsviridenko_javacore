package com.epam.training;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.epam.training.exc.PickFailedException;

public class Contract {
    private Map<Skill, Integer> demands = new HashMap<Skill, Integer>();
    private ArrayList<Team> availableTeams = new ArrayList<Team>();

    public Contract(Map<Skill, Integer> demands, ArrayList<Team> availableTeams) {
	if (demands == null)
	    throw new IllegalArgumentException("map of demands can't be null");
	if (availableTeams == null)
	    throw new IllegalArgumentException("availableTeams can't be null");

	this.demands = demands;
	this.availableTeams = availableTeams;
    }

    public Contract(Map<Skill, Integer> demands) {
	if (demands == null)
	    throw new IllegalArgumentException("map of demands can't be null");

	this.demands = demands;
    }

    public Contract() {
    }

    public Map<Skill, Integer> getDemands() {
	return this.demands;
    }

    public ArrayList<Team> availableTeams() {
	return availableTeams;
    }

    public void addDemand(Skill newSkill, int skillCounter) {
	if (!demands.containsKey(newSkill)) {
	    demands.put(newSkill, skillCounter);
	} else {
	    demands.put(newSkill, demands.get(newSkill) + skillCounter);
	}
    }

    public void addTeam(Team team) {
	if (team != null) {
	    availableTeams.add(team);
	} else {
	    throw new IllegalArgumentException("team can't be null");
	}
    }

    public Team getCheaperTeam(ArrayList<Team> teams) {
	Team cheaperTeam = teams.get(0);
	for (int i = 0; i < teams.size(); i++) {
	    if (cheaperTeam.getPrice() > teams.get(i).getPrice()) {
		cheaperTeam = teams.get(i);
	    }
	}
	return cheaperTeam;
    }

    public Team pickTeamForWork() throws PickFailedException {
	ArrayList<Team> suitableTeams = new ArrayList<Team>();
	for (Team team : availableTeams) {
	    boolean isSuitableTeam = true;
	    Map<Skill, Integer> teamSkills = team.getTeamSkills();

	    for (Skill skill : demands.keySet()) {
		if (teamSkills.containsKey(skill)) {
		    if (demands.get(skill) > teamSkills.get(skill)) {
			isSuitableTeam = false;
			break;
		    }
		} else {
		    isSuitableTeam = false;
		    break;
		}
	    }
	    if (isSuitableTeam) {
		suitableTeams.add(team);
	    }
	}

	if (suitableTeams.size() == 0) {
	    throw new PickFailedException("Has no suitable team for this contract");
	}

	Team result;
	if (suitableTeams.size() == 1) {
	    result = suitableTeams.get(0);
	} else {
	    result = getCheaperTeam(suitableTeams);
	}
	return result;
    }

    private String availableTeamsToString() {
	String result = "";
	for (Team team : availableTeams) {
	    result += "\n[" + team.toString() + "]";
	}
	return result;
    }

    @Override
    public String toString() {
	String string = "Demands: " + this.demands + ";\n" + "AvailableTeams: " + availableTeamsToString();
	return string;
    }
}
