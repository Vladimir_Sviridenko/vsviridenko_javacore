package com.epam.training;

import java.util.Set;
import java.util.TreeSet;

public class Worker {
    private String name;
    private double price;
    private Set<Skill> skills = new TreeSet<Skill>();

    public String getName() {
	return this.name;
    }

    public double getPrice() {
	return this.price;
    }

    public Set<Skill> getSkills() {
	return this.skills;
    }

    public Worker(String name, double price, Set<Skill> skills) {
	if (name == null || name.isEmpty())
	    throw new IllegalArgumentException("name can't be null or empty");
	if (price <= 0)
	    throw new IllegalArgumentException("price can't be 0 or negative");
	if (skills == null)
	    throw new IllegalArgumentException("skills can't be null");

	this.name = name;
	this.price = price;
	this.skills = skills;
    }

    public Worker(String name, double price) {
	if (name == null || name.isEmpty())
	    throw new IllegalArgumentException("name can't be null or empty");
	if (price <= 0)
	    throw new IllegalArgumentException("price must be 0 or negative");

	this.name = name;
	this.price = price;
    }

    public void addSkill(Skill skill) {
	if (skill != null) {
	    skills.add(skill);
	} else {
	    throw new IllegalArgumentException("skill can't be null");
	}
    }

    @Override
    public String toString() {
	String string = "Name: " + this.name.toString() + "; Price: " + this.price + "; Skills:" + this.getSkills();
	return string;
    }
}
