package com.epam.training;

import com.epam.training.exc.PickFailedException;

public class Launcher {
    public static void main(String[] args) {
	// CREATE workers
	Worker w1 = new Worker("Smith", 15);
	Worker w2 = new Worker("Smith", 16);
	Worker w3 = new Worker("Smith", 26);
	Worker w4 = new Worker("Smith", 50);
	Worker w5 = new Worker("Smith", 30);

	// Train Workers
	w1.addSkill(Skill.ECONOMIST);
	w1.addSkill(Skill.TECHNOLOGIST);
	w1.addSkill(Skill.PAINTER);
	w1.addSkill(Skill.ARCHITECTOR);
	w2.addSkill(Skill.DESIGNER);
	w2.addSkill(Skill.PAINTER);

	w3.addSkill(Skill.SURVEYOR);
	w4.addSkill(Skill.LOADER);
	w5.addSkill(Skill.DRIVER);
	w5.addSkill(Skill.CRANE_OPERATOR);

	// CREATE teams
	System.out.println("Teams:");
	Team t1 = new Team();
	t1.addWorker(w1);
	t1.addWorker(w2);
	System.out.println(t1.toString());

	Team t2 = new Team();
	t2.addWorker(w3);
	t2.addWorker(w4);
	t2.addWorker(w5);
	System.out.println(t2.toString());

	// CREATE contract
	Contract contract = new Contract();
	// add Demands to contract
	contract.addDemand(Skill.ECONOMIST, 1);
	contract.addDemand(Skill.ARCHITECTOR, 1);
	contract.addDemand(Skill.DESIGNER, 1);
	contract.addDemand(Skill.PAINTER, 2);

	// add available teams to contract
	contract.addTeam(t1);
	contract.addTeam(t2);
	System.out.println("\nContract Information: ");
	System.out.println(contract.toString());

	// pick suitable team
	try {
	    System.out.println("PickedTeam: " + contract.pickTeamForWork().toString());
	} catch (PickFailedException e) {
	    System.out.println(e.getMessage());
	    contract = null; //close contract
	}
    }
}
