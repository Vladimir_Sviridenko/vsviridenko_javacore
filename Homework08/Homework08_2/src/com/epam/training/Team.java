package com.epam.training;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Team {
    private ArrayList<Worker> workers = new ArrayList<Worker>();

    public Team(ArrayList<Worker> workers) {
	if (workers == null)
	    throw new IllegalArgumentException("workers can't be null");

	this.workers = workers;
    }

    public Team() {
    }

    public double getPrice() {

	double teamPrice = 0;

	for (Worker worker : workers) {
	    teamPrice += worker.getPrice();
	}
	return teamPrice;
    }

    public void addWorker(Worker worker) {
	if (worker != null) {
	    workers.add(worker);
	} else {
	    throw new IllegalArgumentException("worker can't be null");
	}
    }

    public Map<Skill, Integer> getTeamSkills() {
	Map<Skill, Integer> teamCapabilities = new HashMap<Skill, Integer>();

	int workersWithSkillCounter = 0;
	ArrayList<Skill> skills = getSkillsList();

	for (Skill skill : Skill.values()) {
	    workersWithSkillCounter = 0;
	    for (Skill workerSkill : skills) {
		if (skill.equals(workerSkill)) {
		    workersWithSkillCounter++;
		}
	    }
	    if (workersWithSkillCounter != 0) {
		teamCapabilities.put(skill, workersWithSkillCounter);
	    }
	}
	return teamCapabilities;
    }

    private ArrayList<Skill> getSkillsList() {
	ArrayList<Skill> teamSkills = new ArrayList<Skill>();
	for (Worker worker : this.workers) {
	    teamSkills.addAll(worker.getSkills());
	}
	return teamSkills;
    }

    @Override
    public String toString() {
	String string = "Price: " + getPrice() + "; TeamSkills: " + getTeamSkills().toString();
	return string;
    }
}
