package com.epam.training;

import org.junit.Assert;
import org.junit.Test;

import com.epam.training.exc.PickFailedException;

public class ContractTest {

    @Test
    public void testPickTeamForWork() throws PickFailedException {
	// CREATE workers
	Worker w1 = new Worker("Smith", 15);
	Worker w2 = new Worker("Smith", 16);
	Worker w3 = new Worker("Smith", 26);
	Worker w4 = new Worker("Smith", 50);
	Worker w5 = new Worker("Smith", 30);

	// Train Workers
	w1.addSkill(Skill.ECONOMIST);
	w1.addSkill(Skill.PAINTER);
	w1.addSkill(Skill.ARCHITECTOR);
	w2.addSkill(Skill.DESIGNER);
	w2.addSkill(Skill.PAINTER);

	w3.addSkill(Skill.SURVEYOR);
	w4.addSkill(Skill.LOADER);
	w5.addSkill(Skill.DRIVER);
	w5.addSkill(Skill.CRANE_OPERATOR);

	// CREATE teams
	Team t1 = new Team();
	t1.addWorker(w1);
	t1.addWorker(w2);

	Team t2 = new Team();
	t2.addWorker(w3);
	t2.addWorker(w4);
	t2.addWorker(w5);

	// CREATE contract
	Contract contract = new Contract();
	// add Demands to contract
	contract.addDemand(Skill.ECONOMIST, 1);
	contract.addDemand(Skill.ARCHITECTOR, 1);
	contract.addDemand(Skill.DESIGNER, 1);
	contract.addDemand(Skill.PAINTER, 2);

	// add Available teams to contract
	contract.addTeam(t1);
	contract.addTeam(t2);

	// pick best team
	Assert.assertEquals(t1.toString(), contract.pickTeamForWork().toString());
    }

    @Test
    public void testPickTeamForWork_SameSkils_DifferentPrice() throws PickFailedException {
	// CREATE workers
	Worker w1 = new Worker("Smith", 1000);
	Worker w2 = new Worker("Smith", 20);
	Worker w3 = new Worker("Smith", 26);
	Worker w4 = new Worker("Smith", 50);
	Worker w5 = new Worker("Smith", 30);

	// Train Workers
	w1.addSkill(Skill.ECONOMIST);
	w1.addSkill(Skill.ARCHITECTOR);
	w2.addSkill(Skill.DESIGNER);

	w3.addSkill(Skill.ECONOMIST);
	w4.addSkill(Skill.ARCHITECTOR);
	w5.addSkill(Skill.DESIGNER);

	// CREATE teams
	Team t1 = new Team();
	t1.addWorker(w1);
	t1.addWorker(w2);

	Team t2 = new Team();
	t2.addWorker(w3);
	t2.addWorker(w4);
	t2.addWorker(w5);

	// CREATE contract
	Contract contract = new Contract();
	// add Demands to contract
	contract.addDemand(Skill.ECONOMIST, 1);
	contract.addDemand(Skill.ARCHITECTOR, 1);
	contract.addDemand(Skill.DESIGNER, 1);

	// add Available teams to contract
	contract.addTeam(t1);
	contract.addTeam(t2);

	// pick best team
	Assert.assertEquals(t2.toString(), contract.pickTeamForWork().toString());
    }

    @Test(expected = PickFailedException.class)
    public void testPickTeamForWork_Exception() throws PickFailedException {
	// CREATE workers
	Worker w1 = new Worker("Smith", 41);
	Worker w2 = new Worker("Smith", 20);
	Worker w3 = new Worker("Smith", 26);
	Worker w4 = new Worker("Smith", 50);
	Worker w5 = new Worker("Smith", 30);

	// Train Workers
	w1.addSkill(Skill.ARCHITECTOR);
	w2.addSkill(Skill.DESIGNER);

	w3.addSkill(Skill.ECONOMIST);
	w4.addSkill(Skill.ARCHITECTOR);
	w5.addSkill(Skill.DESIGNER);

	// CREATE teams
	Team t1 = new Team();
	t1.addWorker(w1);
	t1.addWorker(w2);

	Team t2 = new Team();
	t2.addWorker(w3);
	t2.addWorker(w4);
	t2.addWorker(w5);

	// CREATE contract
	Contract contract = new Contract();
	// add Demands to contract
	contract.addDemand(Skill.ECONOMIST, 1);
	contract.addDemand(Skill.ARCHITECTOR, 1);
	contract.addDemand(Skill.DESIGNER, 3);

	// add Available teams to contract
	contract.addTeam(t1);
	contract.addTeam(t2);

	// pick best team
	contract.pickTeamForWork();
    }
}
