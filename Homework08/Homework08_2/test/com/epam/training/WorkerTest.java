package com.epam.training;

import java.util.Set;
import java.util.TreeSet;

import org.junit.Assert;
import org.junit.Test;

public class WorkerTest {
    @Test
    public void testAddSkill() {

	Set<Skill> skills = new TreeSet<Skill>();
	skills.add(Skill.LOADER);
	skills.add(Skill.ARCHITECTOR);

	Worker workerExpected = new Worker("Smith", 15, skills);

	Worker workerActual = new Worker("Smith", 15);
	workerActual.addSkill(Skill.LOADER);
	workerActual.addSkill(Skill.ARCHITECTOR);

	Assert.assertEquals(workerExpected.toString(), workerActual.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddSkill_Exception() {
	Worker worker = new Worker("Smith", 15);
	worker.addSkill(null);
    }
}
